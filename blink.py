import machine
import time


pin = machine.Pin(1, machine.Pin.OUT)

pin.on()
print("ON")
time.sleep(1)
pin.off()
print("OFF")
time.sleep(1)
pin.on()
print("ON")
time.sleep(1)
pin.off()
print("OFF")
time.sleep(1)
pin.on()
print("ON")
time.sleep(1)
pin.off()
print("OFF")
