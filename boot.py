import gc
import machine
import network
import upip


def connect_wlan(ssid, password):
    """Connects build-in WLAN interface to the network.
    Args:
        ssid: Service name of Wi-Fi network.
        password: Password for that Wi-Fi network.
    Returns:
        True for success, Exception otherwise.
    """
    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    sta_if.active(True)
    ap_if.active(False)

    if not sta_if.isconnected():
        print("Connecting to WLAN ({})...".format(ssid))
        sta_if.active(False)    # Close any connection
        sta_if.active(True)
        sta_if.config(reconnects=5)     # 5 tries max
        sta_if.connect(ssid, password)
        while not sta_if.isconnected():
            pass

        print('network config:', sta_if.ifconfig())

    return True


def cleanup():
    gc.collect()
    gc.enable()
    print(str(gc.mem_free()) + " free")


def main():
    """Main function. Runs after board boot, before main.py
    Connects to Wi-Fi and checks for latest OTA version.
    """

    cleanup()

    # Wi-Fi credentials
    ssid = "Router72"
    password = "a52fd7a59b47d9f8b9f6e9e5f5e123c3"

    connect_wlan(ssid, password)

    # Install Senko from PyPi
    upip.install("micropython-senko")

    import senko

    cleanup()

    # OTA = senko.Senko(None, None, url="https://github.com/RangerDigital/senko/blob/master/examples/", branch=None,
    # working_dir=None, files=["main.py", "boot.py"])

    #OTA = senko.Senko(user="RangerDigital", repo="senko", working_dir="examples", files=["main.py"])
    OTA = senko.Senko(user=None, repo=None, working_dir=None, url="https://gitlab.com/damped/micopyotatest/-/raw/main", files=["main.py"])

    cleanup()
    if OTA.fetch():
        print("A newer version is available!")
    else:
        print("Up to date!")

    cleanup()

    if OTA.update():
        print("Updated to the latest version! Rebooting...")
        machine.reset()


if __name__ == "__main__":
    main()
