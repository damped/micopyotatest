

try:
    import uasyncio as asyncio
except ImportError:
    import asyncio
from microdot_asyncio import Microdot, Response

app = Microdot()

htmldoc = '''<!DOCTYPE html>
<html lang="en">
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        
        <title>Microdot Example Page</title>
    </head>
    <body class="text-center">
        <div>
            <h1>Microdot Example Page</h1>
            <p>Hello from Microdot!</p>
            <img src="/static/img/500x500_cover.jpg"> 
            <p><a href="/shutdown">Click to shutdown the server</a></p>
        </div>


	  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	  <!-- Icons -->
	  <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>

    </body>
</html>
'''


@app.route('/')
async def root(request):
    return Response(body=htmldoc, headers={'Content-Type': 'text/html'})

@app.route('static/<path:path>')
async def static(request, path):
    """
    expose the static dir

    watch for paths that can expose secret credentals
    """
    import os
    print(os.listdir('static/img'))

    return Response.send_file(filename=('static/'+path), status_code=200)

@app.route('/shutdown')
async def shutdown(request):
    request.app.shutdown()
    return 'The server is shutting down...'


async def main():
    await app.start_server(debug=True)


asyncio.run(main())

