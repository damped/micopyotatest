# boot.py
import senko

GITHUB_URL = "https://gitlab.com/damped/micopyotatest/-/raw/main/"
OTA = senko.Senko(url=GITHUB_URL, files=["boot.py", "main.py"])



def do_connect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.config(dhcp_hostname="test")
        sta_if.connect('Router72', 'a52fd7a59b47d9f8b9f6e9e5f5e123c3')
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())


do_connect()

